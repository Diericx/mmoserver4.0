package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/vova616/chipmunk/vect"
	"golang.org/x/net/websocket"
)

type Server struct {
	addr string
}

type Socket struct {
	io.ReadWriter
	done   chan bool
	closed bool
}

type SendMessage struct {
	Objects     []Data
	Cp          CurrentPlayer
	Leaderboard [5]LeaderboardEntry
}

type LeaderboardEntry struct {
	Username string
	Score    int
}

type ReceiveMessage struct {
	Action     string
	Token      string
	Uid        string
	Value      string
	MouseAngle float64
	MovX       int
	MovY       int
}

type TargetData struct {
	Distance int
	Angle    float64
}

type Data struct {
	Id        string
	Username  string
	Parent    string //id
	Child     string //id
	Height    int
	Type      string
	Tag       string
	Health    float64
	HealthCap int
	Power     int
	MaxPower  int
	X         float64
	Y         float64
	Rot       float64
	Items     []Item
}

type CurrentPlayer struct {
	Id         string
	Username   string
	Parent     string
	Child      string
	Height     int
	Score      int
	Health     float64
	HealthCap  int
	Power      int
	MaxPower   int
	X          float64
	Y          float64
	Rot        float64
	MouseAngle float64
	Items      []string
	Targets    []TargetData
}

type PlayerDataObject struct {
	action       string
	packetObj    ReceiveMessage
	packetString string
	socket       Socket
	player       *Entity
}

var firebaseUrl = "https://diericx.firebaseIO.com/"

//---
//SOCKET---
//---
func (s Socket) Close() error {
	s.done <- true

	s.closed = true

	return nil
}

func socketHandler(ws *websocket.Conn) {
	println("Got connection!")
	s := Socket{ws, make(chan bool), false}
	//add the new player to the data stream so it will be created
	serverInput <- PlayerDataObject{action: "newPlayer", socket: s}

	<-s.done
}

//---
//-----
//----

func GetData(p *Entity) {
	for !p.socket.closed {
		//serialize the packet
		buf := make([]byte, 500)
		n, err := p.socket.Read(buf)
		if err != nil {
			println("Player <", p.username, "> disconnected")
			p.socket.Close()
			p.value = 0
			//p.removeSelf()

			return
		}

		var msg ReceiveMessage
		json.Unmarshal(buf[0:n], &msg)
		//send the data to the stream to be used later
		serverInput <- PlayerDataObject{action: "updatePlayer", player: p, packetObj: msg}
	}
}

func NewServer(addr string) *Server {
	newServer := new(Server)
	newServer.addr = addr
	return newServer
}

func (s Server) listenForPlayers() {
	println("waiting for connection...")
	http.Handle("/", websocket.Handler(socketHandler))
	http.ListenAndServe(s.addr, nil) //192.168.2.36
}

//---Process Data---//
func processServerInput() {
	if len(serverInput) == 0 {
		return
	}

	for len(serverInput) > 0 {
		var serverInputObj = <-serverInput
		if serverInputObj.action == "newPlayer" {
			println("New Player...")
			var p = NewPlayer(serverInputObj.socket, vect.Vect{X: 0, Y: 0}, vect.Vect{X: 40, Y: 40})
			go GetData(p)
		} else if serverInputObj.action == "updatePlayer" {
			var p = serverInputObj.player
			var msg = serverInputObj.packetObj

			if msg.Action == "update" {
				if !p.active {
					p.authorize(firebaseUrl+"users/"+msg.Uid, msg.Token)
				}
				//println(msg.MouseAngle)
				p.body.SetAngle(vect.Float(msg.MouseAngle))
				p.stabalize()
				p.movX = msg.MovX
				p.movY = msg.MovY
			} else if msg.Action == "shoot" {
				p.shoot(nil)
			} else if msg.Action == "shoot-alt" {
				p.shootAlt()
			} else if msg.Action == "drop" {
				if len(p.items) > 0 {
					p.dropItem(0)
				}
			} else if msg.Action == "setParent" {
				var e = findEntityById(msg.Value)
				if e != nil {
					p.setParent(e)
				}
			} else if msg.Action == "equipItem" {
				var p = serverInputObj.player
				i, _ := strconv.Atoi(msg.Value)
				p.equippedItems[0] = p.items[i]
				p.items[i] = Item{name: ""}
			}
		}
	}
}

func processServerOutput() {
	for _, p := range players {
		if !p.active {
			continue
		}

		var playerDataObj PlayerDataObject
		var objects = []Data{}
		var cp = CurrentPlayer{}
		var lb = [5]LeaderboardEntry{}
		var targets []TargetData

		var keys = p.getNearbyKeys()

		for _, key := range keys {
			for _, e := range m[key] {
				if p != e && e != nil {

					//don't send players without a username yet
					if e.entityType == "Player" {
						if e.username == "" {
							continue
						}
					}

					var object Data
					//set parent and child if they are there
					if e.parent != nil {
						object.Parent = e.parent.id.String()
					}
					if e.child != nil {
						object.Child = e.child.id.String()
					}
					object.Height = e.height

					object.Id = e.id.String()
					object.Username = e.username
					object.Type = e.entityType
					object.Tag = e.tag
					object.Health = e.health
					object.HealthCap = e.healthCap
					object.Power = e.power
					object.X = float64(e.body.Position().X)
					object.Y = float64(e.body.Position().Y)
					object.Rot = float64(e.body.Angle())
					objects = append(objects, object)
				}
			}
		}

		//calculate angles
		if target != nil {
			var newTargetData = TargetData{Distance: int(p.distanceTo(target)), Angle: p.lookAt(target)}
			targets = append(targets, newTargetData)
		}

		//set parent and child if they are there
		if p.parent != nil {
			cp.Parent = p.parent.id.String()
		}
		if p.child != nil {
			cp.Child = p.child.id.String()
		}
		cp.Height = p.height

		cp.Id = p.id.String()
		cp.Username = p.username
		cp.Score = int(p.score)
		cp.Health = p.health
		cp.HealthCap = p.healthCap
		cp.Power = p.power
		cp.MaxPower = p.powerMax
		cp.X = float64(p.body.Position().X)
		cp.Y = float64(p.body.Position().Y)
		cp.Rot = float64(p.body.Angle())
		cp.Targets = targets

		for _, i := range p.items {
			cp.Items = append(cp.Items, i.name)
		}

		//populate leaderboard
		for k, v := range leaderboard {
			if v != nil {
				lb[k] = LeaderboardEntry{Username: v.username, Score: int(v.score)}
			}
		}

		m := SendMessage{
			objects,
			cp,
			lb,
		}

		b, _ := json.Marshal(m)
		playerDataObj.player = p
		playerDataObj.action = "sendData"
		playerDataObj.packetString = string(b)
		serverOutput <- playerDataObj
	}
}

func sendServerOutput() {
	//if there is nothing to send, return
	if len(serverOutput) == 0 {
		return
	}
	//if there is something to send, loop through and send all
	for len(serverOutput) > 0 {
		var serverOutputObj = <-serverOutput
		if serverOutputObj.action == "sendData" {
			fmt.Fprint(serverOutputObj.player.socket, string(serverOutputObj.packetString))
		}
	}
}
