package main

import (
	"math"

	"github.com/melvinmt/firebase"
	"github.com/vova616/chipmunk"
	"github.com/vova616/chipmunk/vect"
)

type FirebaseUserData struct {
	Username string
}

var players []*Entity

var DEFAULT_PLAYER_SHOOT_TIME float64 = 100
var DEFAULT_PLAYER_SPEED float64 = 500
var DEFAULT_PLAYER_MAX_SPEED float64 = 500

func NewPlayer(s Socket, location vect.Vect, size vect.Vect) *Entity {
	newPlayer := NewEntity(nil, location, size)
	newPlayer.entityType = "Player"
	newPlayer.body.UserData = newPlayer
	newPlayer.body.Damping = vect.Float(0.98) //0.98
	newPlayer.value = 1
	newPlayer.socket = s
	newPlayer.movX = 0
	newPlayer.movY = 0
	newPlayer.speed = DEFAULT_PLAYER_SPEED
	newPlayer.maxSpeed = DEFAULT_PLAYER_MAX_SPEED
	newPlayer.speedDamp = vect.Float(3)
	newPlayer.shootTime = 100
	newPlayer.shootCooldown = 0
	newPlayer.active = false

	newPlayer.shape = chipmunk.NewBox(vect.Vect{X: 0, Y: 0}, 300, 300)
	newPlayer.shape.IsSensor = true
	newPlayer.shape.SetElasticity(0.95)
	//newPlayer.body.Shapes = make([]*chipmunk.Shape, 0)
	//newPlayer.body.AddShape(newPlayer.shape)

	newPlayer.shoot = newPlayer.playerShoot
	newPlayer.onUpdate = func() {
		var vel = newPlayer.body.Velocity()
		//set player shit to default
		newPlayer.shootTime = DEFAULT_PLAYER_SHOOT_TIME
		newPlayer.speed = DEFAULT_PLAYER_SPEED
		newPlayer.maxSpeed = DEFAULT_PLAYER_MAX_SPEED
		//Edit player according to items
		for _, item := range newPlayer.equippedItems {
			newPlayer.shootTime += item.buff.fireRate
			newPlayer.speed += item.buff.speed
		}
		//update score
		if newPlayer.score > 0 {
			newPlayer.score -= 0.05
		} else {
			newPlayer.score = 0
		}
		//Update leaderboard
		var foundSelf = false
		for i := 0; i < len(leaderboard); i++ {
			if newPlayer.username == "" {
				break
			}
			if leaderboard[i] == newPlayer {
				if foundSelf {
					leaderboard[i] = nil
				}
				foundSelf = true
				continue
			}
			if foundSelf {
				continue
			}
			if leaderboard[i] == nil {
				if !foundSelf {
					leaderboard[i] = newPlayer
				}
				break
			} else {
				if newPlayer.score > leaderboard[i].score {
					leaderboard[i] = newPlayer
					break
				}
			}
		}
		//check value
		if newPlayer.value == 0 {
			//DIE
			newPlayer.dropAllItems()
			newPlayer.removeFromLeaderboard()
		}
		//check health
		if newPlayer.health <= 0 {
			//Drop shit
			newPlayer.dropAllItems()

			//remove from chain
			newPlayer.removeFromChain()

			//deal out score
			for k, v := range newPlayer.damageTaken {
				//if the player hasn't left... add to its score
				if entities[k] != nil {
					entities[k].score += v
				}
			}

			//DIE
			newPlayer.health = 100
			newPlayer.score = 0
			newPlayer.damageTaken = make(map[string]float64)
			newPlayer.body.SetPosition(vect.Vect{X: 0, Y: 0})
		}
		//check power
		if newPlayer.power < newPlayer.powerMax {
			newPlayer.power++
		}
		if newPlayer.power >= newPlayer.powerMax {
			newPlayer.power = newPlayer.powerMax
		}
		//Add players force
		newPlayer.body.AddForce(float32(float64(newPlayer.movX)*newPlayer.speed), float32(float64(newPlayer.movY)*newPlayer.speed))
		//make sure player doesn't go over speed limit
		if float64(vel.X) > newPlayer.maxSpeed {
			vel.X = vect.Float(newPlayer.maxSpeed)
		}
		if float64(vel.X) < -newPlayer.maxSpeed {
			vel.X = vect.Float(-newPlayer.maxSpeed)
		}
		if float64(vel.Y) > newPlayer.maxSpeed {
			vel.Y = vect.Float(newPlayer.maxSpeed)
		}
		if float64(vel.Y) < -newPlayer.maxSpeed {
			vel.Y = vect.Float(-newPlayer.maxSpeed)
		}
		//make sure player stays in bounds
		var playerPos = newPlayer.body.Position()
		if playerPos.X < -WORLD_LIMIT {
			playerPos.X = -WORLD_LIMIT
			vel.X = 0
		}
		if playerPos.X > WORLD_LIMIT {
			playerPos.X = WORLD_LIMIT
			vel.X = 0
		}
		if playerPos.Y < -WORLD_LIMIT {
			playerPos.Y = -WORLD_LIMIT
			vel.Y = 0
		}
		if playerPos.Y > WORLD_LIMIT {
			playerPos.Y = WORLD_LIMIT
			vel.Y = 0
		}

		//set player's velocity and Position
		newPlayer.body.SetPosition(playerPos)
		newPlayer.body.SetVelocity(float32(vel.X), float32(vel.Y))

		if newPlayer.shootCooldown > 0 {
			newPlayer.shootCooldown -= 10
		}

		if newPlayer.shootCooldown < 0 {
			newPlayer.shootCooldown = 0
		}
	}

	players = append(players, newPlayer)

	return newPlayer
}

func (p *Entity) authorize(url string, token string) {
	//ref := firebase.NewReference(url).Auth(token)
	// f := firego.New(url, nil)
	// var v map[string]interface{}
	// if err := f.Value(&v); err != nil {
	// 	println(err)
	// }
	// p.active = true
	// fmt.Printf("%s\n", v["username"])
	// println(v["username"])
	ref := firebase.NewReference(url).Auth(token).Export(false)
	response := FirebaseUserData{}
	if err := ref.Value(&response); err != nil {
		println(err)
	}
	p.username = response.Username
	p.active = true
}

func (p *Entity) playerShoot(e *Entity) {
	if p.shootCooldown != 0 {
		return
	}

	var damageVect = vect.Vect{X: vect.Float(5 * (p.height + 1)), Y: vect.Float(10 * (p.height + 1))}
	var newBullet = NewBullet(vect.Vect{X: 0, Y: 0}, vect.Vect{X: 8, Y: 16}, 60, damageVect)
	newBullet.body.SetPosition(p.body.Position())
	newBullet.body.SetAngle(p.body.Angle() + (math.Pi / 2))
	newBullet.body.SetVelocity(float32(math.Cos(float64(p.body.Angle()))*500), float32(math.Sin(float64(p.body.Angle()))*500))
	newBullet.origin = p

	// if len(p.items) > 0 {
	// 	newBullet.tag = p.items[0].name
	// } else {
	// 	newBullet.tag = "default-bullet"
	// }

	if p.equippedItems[0].name != "" {
		newBullet.tag = p.equippedItems[0].name
	} else {
		newBullet.tag = "default-bullet"
	}

	p.shootCooldown = p.shootTime
}

func (p *Entity) shootAlt() {
	if p.power != 200 {
		return
	}

	var height = 500

	var posX = math.Cos(float64(p.body.Angle())) * float64(height/2)
	var posY = math.Sin(float64(p.body.Angle())) * float64(height/2)

	posX += float64(p.body.Position().X)
	posY += float64(p.body.Position().Y)

	var damageVect = vect.Vect{X: 40, Y: 60}
	var newBullet = NewBullet(vect.Vect{X: 0, Y: 0}, vect.Vect{X: 2, Y: vect.Float(height)}, 5, damageVect)
	newBullet.body.SetPosition(vect.Vect{X: vect.Float(posX), Y: vect.Float(posY)})
	newBullet.body.SetAngle(p.body.Angle() + (math.Pi / 2))
	newBullet.tag = "Laser MKI"
	//newBullet.entity.body.SetVelocity(float32(math.Cos(float64(p.entity.body.Angle()))*500), float32(math.Sin(float64(p.entity.body.Angle()))*500))
	newBullet.origin = p

	//update laser's oncollision so it doesnt remove
	newBullet.onCollide = nil
	newBullet.onCollide = func(e *Entity) {
		if e != newBullet.origin {
			e.takeDamage(newBullet)
		}
	}

	p.power = 0
}

func (p *Entity) dropItem(i int) {
	if p.items[i].name != "" {
		pos := vect.Vect{X: p.body.Position().X - 20, Y: p.body.Position().Y - 20}
		obj := NewItem(pos)
		target = obj
		obj.items[0] = p.items[i]
		obj.tag = obj.items[0].name
		println("dropped: ", obj.items[0].name)
		p.items[i] = Item{name: ""}
	}

	// pos := vect.Vect{X: p.body.Position().X - 20, Y: p.body.Position().Y - 20}
	// obj := NewItem(pos)
	// target = obj
	// obj.items = Item{} //append(obj.items, p.items[0])
	// obj.tag = obj.items[0].name
	// p.items = append(p.items[:0], p.items[0+1:]...)
}

func (p *Entity) dropAllItems() {
	for i := 0; i < len(p.items); i++ {
		p.dropItem(i)
	}
}

func (p *Entity) stabalize() {
	p.body.SetAngularVelocity(0)
}
