package main

type Buff struct {
    damage float64
    health float64
    fireRate float64
    speed float64
}
