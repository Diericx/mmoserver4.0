package main

import (
	"time"

	"github.com/pkg/profile"
	"github.com/vova616/chipmunk/vect"
	//"github.com/vova616/chipmunk/vect"
)

// var alive = true
//
var WORLD_LIMIT = vect.Float(1000)
var FRAME_WAIT_TIME float64 = 16

var serverInput = make(chan PlayerDataObject, 1000)

var serverOutput = make(chan PlayerDataObject, 1000)

var physics = NewPhysics()

func main() {

	defer profile.Start(cfg).Stop()

	var server = NewServer(":7777") //192.168.2.36

	//-----create items in the world-------

	//create new item
	NewItem(vect.Vect{X: -50, Y: -50})

	//create eyeball
	NewEyeBoss(vect.Vect{X: 750, Y: 750})

	NewDickEnemy(vect.Vect{X: -700, Y: -700})

	//------Start server ops---------
	go server.listenForPlayers()
	go sendServerOutput()
	//
	initializeEntityManager()

	for {
		w := ForLoopWaiter{start: time.Now()}

		processServerInput()
		physics.stepPhysics()
		updateEntitiesChannels()
		removeEnities()
		updateEntitiesCellData()
		processServerOutput()
		sendServerOutput()

		w.waitForTime(FRAME_WAIT_TIME)
	}

	// //Update Display (ALWAYS ON MAIN THREAD)
	// //InitializeDisplay()
}

//---ForLoopWaiter Definitoin
type ForLoopWaiter struct {
	start time.Time
}

func (flw ForLoopWaiter) waitForTime(maxMilliToWait float64) {
	var deltaTime = time.Since(flw.start)
	var delta = deltaTime.Seconds()
	var deltaMilli = delta * 1000
	deltaMilli = maxMilliToWait - deltaMilli

	if deltaMilli > 0 {
		time.Sleep(time.Duration(deltaMilli) * time.Millisecond)
	}
}
