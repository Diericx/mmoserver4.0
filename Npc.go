package main

import (
	"math"
	"math/rand"

	"github.com/vova616/chipmunk/vect"
)

func NewEyeBoss(loc vect.Vect) *Entity {
	var size = vect.Vect{X: 150, Y: 150}
	var damageVect = vect.Vect{X: 10, Y: 20}
	var speed float32 = 75

	newObj := NewGameObject(loc, size)
	newObj.value = 1
	newObj.health = 500
	newObj.healthCap = 500
	newObj.shootTime = 100
	newObj.shootCooldown = 0
	newObj.setDamage(damageVect)
	newObj.entityType = "Npc"
	newObj.tag = "EyeBoss"
	newObj.body.Damping = vect.Float(0.99)
	newObj.shoot = newObj.shootAtPlayer

	NewEyeEnemy(newObj)
	NewEyeEnemy(newObj)
	NewEyeEnemy(newObj)
	NewEyeEnemy(newObj)
	NewEyeEnemy(newObj)
	NewEyeEnemy(newObj)

	newObj.onCollide = func(e *Entity) {
		if e != newObj.origin && e.entityType == "Player" {
			e.takeDamage(newObj)
		}
	}

	newObj.onRemove = func() {
		var worldLimitFloat = float64(WORLD_LIMIT)
		var randX = (rand.Float64() * (worldLimitFloat * 2)) - worldLimitFloat
		var randY = (rand.Float64() * (worldLimitFloat * 2)) - worldLimitFloat
		NewEyeBoss(vect.Vect{X: vect.Float(randX), Y: vect.Float(randY)})
		println(randX, randY)
	}

	newObj.onUpdate = func() {
		//handle shoot cooldown
		if newObj.shootCooldown > 0 {
			newObj.shootCooldown -= 10
		}
		if newObj.shootCooldown < 0 {
			newObj.shootCooldown = 0
		}
		//else, find a player nearby
		var nearestPlayer = newObj.findNearestPlayer(500)
		if nearestPlayer != nil {
			//move to nearest player
			var loc1 = newObj.body.Position()
			var loc2 = nearestPlayer.body.Position()
			var deltaX = float64(loc2.X - loc1.X)
			var deltaY = float64(loc2.Y - loc1.Y)
			var angle = math.Atan2(deltaY, deltaX)
			var xMov = math.Cos(angle)
			var yMov = math.Sin(angle)
			newObj.body.AddForce(float32(xMov)*speed, float32(yMov)*speed)
			//shoot at nearest player
			if newObj.shootCooldown <= 0 {
				newObj.shoot(nearestPlayer)
				newObj.shootCooldown = newObj.shootTime
			}
		}
	}

	return newObj
}

func NewEyeEnemy(parent *Entity) {
	var size = vect.Vect{X: 69.5, Y: 50}
	var damageVect = vect.Vect{X: 4, Y: 8}
	var speed float32 = 1000

	var PPos = parent.body.Position()
	var posOffset = vect.Vect{X: PPos.X + vect.Float((rand.Float64()*10)-5), Y: PPos.Y + vect.Float((rand.Float64()*10)-5)}

	newObj := NewGameObject(posOffset, size)
	newObj.value = 1
	newObj.health = 100
	newObj.healthCap = 100
	newObj.shootTime = 0
	newObj.shootCooldown = 0
	newObj.setDamage(damageVect)
	newObj.entityType = "Npc"
	newObj.tag = "EyeEnemy"
	newObj.body.Damping = vect.Float(0.989)

	newObj.onCollide = func(e *Entity) {
		if e != newObj.origin && e.entityType == "Player" {
			e.takeDamage(newObj)
		}
	}

	newObj.onUpdate = func() {
		//find a player nearby
		var targetE = newObj.findNearestPlayer(500)
		if targetE == nil || (newObj.distanceTo(parent) > 500) {
			targetE = parent
		}
		//move to nearest player
		var loc1 = newObj.body.Position()
		var loc2 = targetE.body.Position()
		var deltaX = float64(loc2.X - loc1.X)
		var deltaY = float64(loc2.Y - loc1.Y)
		var angle = math.Atan2(deltaY, deltaX)
		newObj.body.SetAngle(vect.Float(angle))
		var xMov = math.Cos(angle)
		var yMov = math.Sin(angle)
		newObj.body.AddForce(float32(xMov)*speed, float32(yMov)*speed)
	}

}

func NewDickEnemy(loc vect.Vect) *Entity {
	var size = vect.Vect{X: 150, Y: 150}
	var damageVect = vect.Vect{X: 15, Y: 30}
	var speed float32 = 175

	newObj := NewGameObject(loc, size)
	newObj.value = 1
	newObj.health = 500
	newObj.healthCap = 500
	newObj.shootTime = 100
	newObj.shootCooldown = 0
	newObj.setDamage(damageVect)
	newObj.entityType = "Npc"
	newObj.tag = "DickEnemy"
	newObj.body.Damping = vect.Float(0.99)
	newObj.shoot = newObj.shootRadial

	newObj.onCollide = func(e *Entity) {
		if e != newObj.origin && e.entityType == "Player" {
			e.takeDamage(newObj)
		}
	}

	newObj.onRemove = func() {
		var worldLimitFloat = float64(WORLD_LIMIT)
		var randX = (rand.Float64() * (worldLimitFloat * 2)) - worldLimitFloat
		var randY = (rand.Float64() * (worldLimitFloat * 2)) - worldLimitFloat
		NewDickEnemy(vect.Vect{X: vect.Float(randX), Y: vect.Float(randY)})
		println(randX, randY)
	}

	newObj.onUpdate = func() {
		//handle shoot cooldown
		if newObj.shootCooldown > 0 {
			newObj.shootCooldown -= 10
		}
		if newObj.shootCooldown < 0 {
			newObj.shootCooldown = 0
		}
		//else, find a player nearby
		var nearestPlayer = newObj.findNearestPlayer(500)
		if nearestPlayer != nil {
			//move to nearest player
			var loc1 = newObj.body.Position()
			var loc2 = nearestPlayer.body.Position()
			var deltaX = float64(loc2.X - loc1.X)
			var deltaY = float64(loc2.Y - loc1.Y)
			var angle = math.Atan2(deltaY, deltaX)
			var xMov = math.Cos(angle)
			var yMov = math.Sin(angle)
			newObj.body.AddForce(float32(xMov)*speed, float32(yMov)*speed)
			//shoot at nearest player
			if newObj.shootCooldown <= 0 {
				newObj.shoot(nearestPlayer)
				newObj.shootCooldown = newObj.shootTime
			}
		}
	}

	return newObj
}

func (e *Entity) shootRadial(other *Entity) {
	for i := 0; i < 8; i++ {
		var angle float64 = ((math.Pi * 2) / float64(8)) * float64(i)
		var damageVect = vect.Vect{X: 5, Y: 10}
		var newBullet = NewBullet(vect.Vect{X: 0, Y: 0}, vect.Vect{X: 8, Y: 16}, 60, damageVect)
		newBullet.body.SetPosition(e.body.Position())
		newBullet.body.SetAngle(vect.Float(angle + math.Pi/2))
		newBullet.body.SetVelocity(float32(math.Cos(angle)*800), float32(math.Sin(angle)*800))
		newBullet.origin = e
		newBullet.tag = "default-bullet"
	}

}

func (e *Entity) shootAtPlayer(other *Entity) {
	var damageVect = vect.Vect{X: 5, Y: 10}
	var newBullet = NewBullet(vect.Vect{X: 0, Y: 0}, vect.Vect{X: 8, Y: 16}, 60, damageVect)
	var angle = e.lookAt(other)
	newBullet.body.SetPosition(e.body.Position())
	newBullet.body.SetAngle(vect.Float(angle + math.Pi/2))
	newBullet.body.SetVelocity(float32(math.Cos(angle)*400), float32(math.Sin(angle)*400))
	newBullet.origin = e
	newBullet.tag = "default-bullet"
}
