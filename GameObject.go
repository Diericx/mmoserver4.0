package main

import (
	//"github.com/vova616/chipmunk"
	"github.com/vova616/chipmunk/vect"
)

var gameObjects []*Entity

//Create DEFAULT gameObject
func NewGameObject(location vect.Vect, size vect.Vect) *Entity {
	newGameObject := NewEntity(nil, location, size)
	newGameObject.body.UserData = newGameObject
	newGameObject.entityType = "GameObject"
	gameObjects = append(gameObjects, newGameObject)
	return newGameObject
}

func NewBullet(loc vect.Vect, size vect.Vect, life int, damageVect vect.Vect) *Entity {
	newGameObject := NewGameObject(loc, size)
	newGameObject.value = life
	newGameObject.setDamage(damageVect)
	newGameObject.entityType = "Bullet"

	newGameObject.onCollide = func(e *Entity) {
		if e != newGameObject.origin && ((e.entityType == "Npc" && newGameObject.origin.entityType != "Npc") || e.entityType == "Player") {
			e.takeDamage(newGameObject)
			newGameObject.value = 0
		}
	}

	newGameObject.onUpdate = func() {
		newGameObject.value--
	}

	return newGameObject
}
