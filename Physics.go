package main

import (
	//"math"

	//"log"
	"github.com/vova616/chipmunk"
	"github.com/vova616/chipmunk/vect"
)

var space *chipmunk.Space

type Physics struct {
}

func NewPhysics() *Physics {
	space = chipmunk.NewSpace()
	//space.Damping = vect.Float(0.2)
	newP := new(Physics)
	return newP
}

func (p Physics) addBody(body *chipmunk.Body) {
	space.AddBody(body)
}

func (phys Physics) stepPhysics() {
	space.Step(vect.Float(1.0 / 60.0))
}
